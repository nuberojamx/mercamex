package org.mercamex.mercamex;

/**
 * Created by pablofabre on 12/12/17.
 */

public class ProductosA {
        String articulo;
        String tipo;
        String cantidad;
        String tit;
        String nombre;
public ProductosA(){
}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public String getTit() {
        return tit;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCantidad() {
        return cantidad;
    }
    // Get, set and constructors are obvious here I am just saving some space in the post

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getArticulo() {
        return articulo;
    }

    public String getTipo() {
        return tipo;
    }
}
