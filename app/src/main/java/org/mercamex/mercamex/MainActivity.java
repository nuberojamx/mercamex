package org.mercamex.mercamex;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent colorsIntent = new Intent(MainActivity.this, Drawee.class);

                // Start the new activity
                startActivity(colorsIntent);
                ImageView image = (ImageView) findViewById(R.id.imageBlink);
                final Animation animation = new AlphaAnimation((float) 1.5, 0); // Change alpha from fully visible to invisible
                animation.setDuration(1200); // duration - half a second
                animation.setInterpolator(new LinearInterpolator()); // do not alter
                // animation
                // rate
                animation.setRepeatCount(Animation.INFINITE); // Repeat animation
                // infinitely
                animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the
                // end so the button will
                // fade back in
                image.startAnimation(animation);
                finish();
            }
        }, 1200);

    }
}
