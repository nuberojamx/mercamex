package org.mercamex.mercamex;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class MainRecyclerSearch extends AppCompatActivity implements SearchView.OnQueryTextListener {

    int[] r_category = {R.drawable.accesorios, R.drawable.articulos_de_limpieza, R.drawable.consumibles,
            R.drawable.equipos, R.drawable.importaciones,R.drawable.integradores_de_pcs,R.drawable.magneticos_y_opticos,
            R.drawable.muebles,R.drawable.papel,R.drawable.papeleria,R.drawable.seguridad, R.drawable.software, R.drawable.varios};
    String[] r_in ={"Accesorios", "Artículos de limpieza", "Consumibles", "Equipos", "Importaciones", "Integradores de PCs", "Magnéticos y Ópticos", "Muebles", "Papel", "Papelería", "Seguridad", "Software", "Varios"};
    RecyclerAdapter adapter;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<SearchResultFirst> arrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycler_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        int count = 0;
        for(String Homo : r_in){
            //String query, String in, String category_image, int results
            arrayList.add(new SearchResultFirst(Homo, r_in[count], r_category[count], 300));
            count++;
        }
        adapter = new RecyclerAdapter(arrayList);
        recyclerView.setAdapter(adapter);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView =(SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
