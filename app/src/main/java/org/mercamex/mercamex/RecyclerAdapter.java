package org.mercamex.mercamex;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pablofabre on 1/26/18.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{
    ArrayList<SearchResultFirst> arrayList = new ArrayList<>();
    RecyclerAdapter(ArrayList<SearchResultFirst> arrayList){
        this.arrayList = arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
holder.r_category.setImageResource((arrayList.get(position).getCategory_image()));
holder.r_in.setText(arrayList.get(position).getIn());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView r_category;
        TextView r_query;
        TextView r_in;

        public MyViewHolder(View itemView) {
            super(itemView);
            r_category = (ImageView) itemView.findViewById(R.id.category);
            r_query = (TextView) itemView.findViewById(R.id.query);
            r_in = (TextView) itemView.findViewById(R.id.in);
        }
    }
    public void setFilter(ArrayList<SearchResultFirst> newList){
        arrayList= new ArrayList<>();
        arrayList.addAll(newList);
    }
}
