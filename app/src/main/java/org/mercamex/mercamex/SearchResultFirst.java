package org.mercamex.mercamex;

/**
 * Created by pablofabre on 1/26/18.
 */

public class SearchResultFirst {
    private String query;
    private String in;
    private int category_image;
    private int results;

    public SearchResultFirst(String query, String in, int category_image, int results) {
        this.setQuery(query);
        this.setCategory_image(category_image);
        this.setIn(in);
        this.setResults(results);

    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

    public String getQuery() {
        return query;
    }

    public String getIn() {
        return in;
    }

    public int getCategory_image() {
        return category_image;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public void setCategory_image(int category_image) {
        this.category_image = category_image;
    }
}


